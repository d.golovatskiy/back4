<?php
header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    if (!empty($_COOKIE['save'])) {
        setcookie('save', '0', 1);
        print('Success!');
    }

    $errors = array();
    $errors['fio'] = !empty($_COOKIE['fio_error']);
    $errors['email'] = !empty($_COOKIE['email_error']);

    if ($errors['fio']) {
        setcookie('fio_error', '', 100000);
        $_COOKIE['fio'] = "";
        print('<div class="error">Заполните имя.</div>');
    }
    if ($errors['email']) {
        setcookie('email_error', '', 100000);
        $_COOKIE['email'] = "";
        print('<div class="error">Заполните e-mail.</div>');
    }

    $NAME = empty($_COOKIE['fio']) ? '' : $_COOKIE['fio'];
    $EMAIL = empty($_COOKIE['email']) ? '' : $_COOKIE['email'];
    $SEX = empty($_COOKIE['sex']) ? '' : $_COOKIE['sex'];
    $YEAR = empty($_COOKIE['year']) ? '' : $_COOKIE['year'];
    $LIMB = empty($_COOKIE['limb']) ? '' : $_COOKIE['limb'];
    $BIO = empty($_COOKIE['bio']) ? '' : $_COOKIE['bio'];
    $GOD = empty($_COOKIE['god']) ? '' : $_COOKIE['god'];
    $CLIP = empty($_COOKIE['clip']) ? '' : $_COOKIE['clip'];
    $FLY = empty($_COOKIE['fly']) ? '' : $_COOKIE['fly'];
    include('form.php');
    exit();
}

$errors = FALSE;
if (empty($_POST['fio'])) {
    setcookie('fio_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
} else {
    setcookie('fio', $_POST['fio'], time() + 30 * 24 * 60 * 60);
}

if (empty($_POST['email'])) {
    setcookie('email_error', '1', time() + 30 * 24 * 60 * 60);
    $errors = TRUE;

} else if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
    setcookie('email_error', '1', time() + 30 * 24 * 60 * 60);
    $errors = TRUE;
} else {
    setcookie('email', $_POST['email'], time() + 30 * 24 * 60 * 60);
}

if ($errors) {
    header('Location: index.php');
    exit();
} else {
    setcookie('fio_error', '', 100000);
    setcookie('email_error', '', 100000);
    setcookie('limb', $_POST['limb'], 0);
    setcookie('sex', $_POST['sex'], 0);
    setcookie('year', $_POST['year'], 0);
    setcookie('god', intval(in_array("ab_god", $_POST['abilities'])), 0);
    setcookie('fly', intval(in_array("ab_fly", $_POST['abilities'])), 0);
    setcookie('clip', intval(in_array("ab_clip", $_POST['abilities'])), 0);
}

setcookie('save', '1', 0);
header('Location: index.php');


    
    $sup= implode(",",$_POST['superpower']);

    $conn = new PDO("mysql:host=localhost;dbname=u40980", 'u40980', '1404971', array(PDO::ATTR_PERSISTENT => true));

    
    $user = $conn->prepare("INSERT INTO form SET name = ?, email = ?, dob = ?, sex = ?, limbs = ?, bio = ?");
    $user -> execute([$_POST['fio'], $_POST['email'],  $_POST['year'], $_POST['sex'], $_POST['limb'], $_POST['bio']]);

?>
